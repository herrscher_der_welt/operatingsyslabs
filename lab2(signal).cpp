#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <strings.h>
#include <netinet/in.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
 
volatile sig_atomic_t wasSigHup = 0;
 
void sigHupHandler(int r)
{
	wasSigHup = 1;
}

int main()
{
	int sock_fd;
	sock_fd = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in addr;
	bzero(&addr, sizeof(struct sockaddr_in));

	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	addr.sin_port = htons(1234); //подключаться можно по 127.0.0.1 1234

	bind(sock_fd, (struct sockaddr*)&addr, sizeof(struct sockaddr_in));
	listen (sock_fd, 512);

	char* buffer = new char[1024];
 
	struct sigaction sa;
	sigaction (SIGHUP, NULL, &sa);
	sa.sa_handler = sigHupHandler;
	sa.sa_flags |= SA_RESTART;
	sigaction (SIGHUP, &sa, NULL);

	sigset_t blockedMask;
	sigset_t origMask;
	sigemptyset(&blockedMask);
	sigaddset (&blockedMask, SIGHUP);
	sigprocmask(SIG_BLOCK, &blockedMask, &origMask);
 
	int res;
	int acc_fd;

	int clients_count_max = 10;
	int clients[10];
	for(int i = 0; i < clients_count_max; i++)
		clients[i] = 0;

	while(!wasSigHup)
	{
		int fd_max = sock_fd;
		int empty_slot = -1;

		fd_set fds;
		FD_ZERO(&fds);
		FD_SET(sock_fd, &fds);
		for (int i = 0; i < clients_count_max; i++)
		{
			if(clients[i] == 0)
			{
				empty_slot = i;
				continue;
			}
			FD_SET(clients[i], &fds);
			if(clients[i] > fd_max)
			{
				fd_max = clients[i];
			}
		}
		
                res = pselect (fd_max + 1, &fds, NULL, NULL, NULL, &origMask);
		if (res < 0 && errno != EINTR)
		{
			perror("select error");
			printf("%d", res);
			exit(1);
		}
		if (wasSigHup)
		{
			puts ("Был получен сигнал. Выход");
			break;
		}

		for(int i = 0; i < clients_count_max; i++)
		{
			if(clients[i] == 0)
				continue;

			if(FD_ISSET(clients[i], &fds))
			{
				char* buffer = new char[512];
				int recv_len = recv(clients[i], buffer, 512, 0);

				if(recv_len == -1)
				{
					puts("Данных не получено");
					continue;
				}

				printf("Клиент отправляет сообщение :");
				puts(buffer);

				for(int i = 0; i < recv_len/2; i++)
				{
					char temp = buffer[i];
					buffer[i] = buffer[recv_len - i - 1];
					buffer[recv_len - i - 1] = temp;
				}

				send(clients[i], buffer, recv_len,0);

				close(clients[i]);
				clients[i] = 0;
				puts("Клиент отключился");
			}
		}

		if (FD_ISSET(sock_fd, &fds))
		{
			acc_fd = accept(sock_fd, (struct sockaddr*)NULL, NULL);
			if (acc_fd < 0)
			{
				perror ("ошибка подключения");
				exit(1);
			}

			if(empty_slot != -1)
			{
				puts("Клиент подключился");
				clients[empty_slot] = acc_fd;
			}
			else
			{
				puts("Максимум клиентов уже подключено");
				close(acc_fd);
			}
		}
	}
 
	return 0;
}

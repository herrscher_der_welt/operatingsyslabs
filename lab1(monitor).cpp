#include <stdio.h>
#include <stdlib.h>
#include <pthread.h> //нужен флаг -pthread при компиляции
#include <zconf.h>
 
pthread_cond_t cond1 = PTHREAD_COND_INITIALIZER;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
bool ready = false;
 
void *provide(void *){
    while(1) { //бесконечный поток-поставщик
        pthread_mutex_lock(&lock);
        if (ready) {
            pthread_mutex_unlock(&lock);
            continue;
        }
        ready = true;
        printf("Ресурс теперь доступен для потребителя\n");
        pthread_cond_signal(&cond1);
        pthread_mutex_unlock(&lock);
        sleep(1);
    }
    return NULL;
}
 
void *consume(void *){
    while(1){ //бесконечный поток-потребитель
        pthread_mutex_lock(&lock);
        while (!ready)
        {
            pthread_cond_wait(&cond1, &lock);
            printf ("Потребитель увидел доступный ресурс\n");
        }
        ready = false;
        printf ("Ресурс поглощен потребителем\n");
        pthread_mutex_unlock(&lock);
    }
    return NULL;
}
 
int main() {
    pthread_t provider;
    pthread_t user;
 
    pthread_create(&provider, NULL, provide, NULL);
    pthread_create(&user, NULL, consume, NULL);

    pthread_join(provider, NULL);
    pthread_join(user, NULL);
 
    return 0;
}
